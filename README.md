# xyz2cube

## Usage

Run the script passing the input file as the first argument and output file as second, e.g.

```
python ./xyz2cube.py fes.dat fes.cube
```

Then run VMD and choose the `isosurface` representation to view the density:
```
vmd fes.cube
```

## Input format

Script will work for any file that has volumetric data written in the following format:

```
X_coord  Y_coord  Z_coord  f(X,Y,Z)
```

Only the first 4 columns are considered (others are discarded), and empty lines or
lines commented out with hashes will be ignored.

Note that while the XYZ grid can span different intervals in individual dimensions,
the grid in each dimension has to be equally spaced, i.e. currently no resampling
is performed on the data if this is not the case.