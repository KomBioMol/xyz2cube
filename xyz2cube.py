import numpy as np
from optparse import OptionParser


class XYZ:
    def __init__(self, opts):
        dtype = [('x', float), ('y', float), ('z', float), ('q', float)]
        xyzq = [tuple(float(x) for x in line.split()[:4]) for line in open(opts.xyzq)
                if line.strip() and line.strip()[0] not in ['#', '@']]
        self.x_vals = sorted(set([tp[0] for tp in xyzq]))
        self.y_vals = sorted(set([tp[1] for tp in xyzq]))
        self.z_vals = sorted(set([tp[2] for tp in xyzq]))
        xyzq = np.array(xyzq, dtype=dtype)
        self.xyzq = np.sort(xyzq, order=['x', 'y', 'z'])
        self.grid_shape = [len(self.x_vals), len(self.y_vals), len(self.z_vals)]
        self.grid_spacing = [[self.x_vals[1] - self.x_vals[0], 0, 0],
                             [0, self.y_vals[1] - self.y_vals[0], 0],
                             [0, 0, self.z_vals[1] - self.z_vals[0]]]
    
    def project_free(self, opts):
        """
        projects the 3D free energies on a 2D plane
        :param opts: parameters passed from the parser
        :return: None
        """
        dims_dict = {"X": 0, "Y": 1, "Z": 2}
        assert len(options.project.strip()) == 2
        dim1, dim2 = sorted([dims_dict[x] for x in options.project.upper()])
        dim_proj = list({0, 1, 2}.difference({dim1, dim2}))[0]
        ulim = float(options.urange) if options.urange else np.infty
        llim = float(options.lrange) if options.lrange else -np.infty
        probs = np.zeros((self.grid_shape[dim1], self.grid_shape[dim2]))
        vals1 = {[self.x_vals, self.y_vals, self.z_vals][dim1][n]: n for n in range(self.grid_shape[dim1])}
        vals2 = {[self.x_vals, self.y_vals, self.z_vals][dim2][n]: n for n in range(self.grid_shape[dim2])}
        for entry in self.xyzq:
            if llim < entry[dim_proj] < ulim:
                probs[vals1[entry[dim1]], vals2[entry[dim2]]] += np.exp(-entry[3] / (0.001987*float(opts.temperature)))
        free = -0.001987*float(opts.temperature)*np.log(probs/np.sum(probs))
        free -= np.max(free)
        np.savetxt("dim1.dat", np.array(sorted(list(vals1.keys()))), fmt="%10.4f")
        np.savetxt("dim2.dat", np.array(sorted(list(vals2.keys()))), fmt="%10.4f")
        np.savetxt("fep2d_{}.dat".format(options.project.strip().lower()), free, fmt="%10.4f")

    def write_cube(self, opts):
        fact = 10 if opts.m else 1
        with open(opts.out_cube, 'w') as out:
            out.write('cube\ncube\n')
            if opts.x is not None and opts.x is not None and opts.x is not None:
                opts.x, opts.y, opts.z = opts.x * fact, opts.y * fact, opts.z * fact
                x_len = (self.grid_shape[0] - 1) * self.grid_spacing[0][0] * fact
                y_len = (self.grid_shape[1] - 1) * self.grid_spacing[1][1] * fact
                z_len = (self.grid_shape[2] - 1) * self.grid_spacing[2][2] * fact
                x, y, z = opts.x - x_len/2, opts.y - y_len/2, opts.z - z_len/2
            else:
                x, y, z = (0, 0, 0)
            out.write("    1{:>12.6f}{:>12.6f}{:>12.6f}\n".format(x, y, z))
            for sh, sp in zip(self.grid_shape, self.grid_spacing):
                out.write("{:>5d}{:>12.6f}{:>12.6f}{:>12.6f}\n".format(sh, *[q*fact for q in sp]))
            out.write("    1    0.000000    0.000000    0.000000    0.000000\n")
            count = 0
            for i in range(self.grid_shape[0]):
                for j in range(self.grid_shape[1]):
                    for k in range(self.grid_shape[2]):
                        out.write("{:12.6f}".format(self.xyzq[count][3]))
                        count += 1
                        if count % 6 == 0:
                            out.write('\n')


def plot_2D():
    import matplotlib.pyplot as plt
    X, Y = np.meshgrid(np.loadtxt('dim1.dat'), np.loadtxt('dim2.dat'))
    try:
        Z = np.loadtxt('fep2d_xy.dat')
    except OSError:
        try:
            Z = np.loadtxt('fep2d_xz.dat')
        except OSError:
            Z = np.loadtxt('fep2d_yz.dat')
    plt.contour(X.T, Y.T, Z, colors='C0', linestyles='solid')
    plt.contourf(X.T, Y.T, Z, cmap='Blues_r')
    plt.colorbar()
    plt.show()
    

def parse_options():
    parser = OptionParser(usage="%prog -f volumetric.dat -o output.cube [-x x_center -y y_center -z z_center]")
    parser.add_option("-f", dest="xyzq", action="store", type="string",
                      help="input file")
    parser.add_option("-o", dest="out_cube", action="store", type="string", default='fes.cube',
                      help="output .cube file")
    parser.add_option("-x", dest="x", action="store", type="float", default=None,
                      help="x coordinates of the grid center")
    parser.add_option("-y", dest="y", action="store", type="float", default=None,
                      help="y coordinates of the grid center")
    parser.add_option("-z", dest="z", action="store", type="float", default=None,
                      help="z coordinates of the grid center")
    parser.add_option("-m", dest="m", action="store_true",
                      help="save in nm")
    parser.add_option("-t", dest="temperature", action="store", type="float", default=300,
                      help="temperature of the system")
    parser.add_option("-p", dest="project", action="store", type="str", default="",
                      help="use this keyword only to generate a 2D projection of the 3D data; "
                           "e.g. 'XY' projects on first and second dimensions")
    parser.add_option("-l", dest="lrange", action="store", type="str", default=None,
                      help="lower range of the projection for the 2D free energy plot")
    parser.add_option("-u", dest="urange", action="store", type="str", default=None,
                      help="upper range of the projection for the 2D free energy plot")
    parser.add_option("-g", dest="graph", action="store_true",
                      help="flag to plot the resulting 2D plot (run again)")
    (opts, args) = parser.parse_args()
    return opts


if __name__ == "__main__":
    options = parse_options()
    if not options.graph:
        q = XYZ(options)
        if not options.project:
            q.write_cube(options)
        else:
            q.project_free(options)
    else:
        plot_2D()
